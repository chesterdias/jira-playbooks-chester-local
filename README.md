# README #

This README would normally document whatever steps are necessary to get your application up and running.

## Ansible Repository for Jira server replication and upgrade ##

This repository list the steps to execute the Jira ansible playbooks.


###Pre-Requisites ###
* boto3
* python3
* pip3
* ansible 2.9 or higher
* AWS access Keys
* JIRA_HOME BACKUP (*.tar.gz) to be present on the control node home path.
* JIRA_SQL backup (sql.gz) to be present on the control node home path.
* * *

###Execution ###
**Step 1:**
Fill the below mentioned files which contain variables used by ansible.
```
group_vars/common.yml
group_vars/secrets.yml
```
**Step 2:**
Execute the playbooks
```
ansible-playbook main.yml
```

* * *
Note: Jira runs on port 8080 
Default JIRA URL: http://<aws_endpoint>:8080/
### Contact ###
**Chester Dias**
